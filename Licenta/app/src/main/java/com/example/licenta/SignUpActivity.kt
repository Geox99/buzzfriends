package com.example.licenta

import android.app.ProgressDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        signin_link_btn.setOnClickListener {
            startActivity(Intent(this,SignInActivity::class.java))
        }

        signup_btn.setOnClickListener {
            CreateAccount()
        }
    }

    private fun CreateAccount() {

        val fullName= fullname_signup.editText?.text.toString()
        val username= username_signup.editText?.text.toString()
        val email= email_signup.editText?.text.toString()
        val password= password_signup.editText?.text.toString()


        when {
            TextUtils.isEmpty(fullName) -> Toast.makeText(this,"full name is required",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(username) -> Toast.makeText(this,"username is required",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(email) -> Toast.makeText(this,"email is required",Toast.LENGTH_LONG).show()
            TextUtils.isEmpty(password) -> Toast.makeText(this,"password is required",Toast.LENGTH_LONG).show()

            else -> {
                val progressDialog= ProgressDialog(this@SignUpActivity)
                progressDialog.setTitle("Sign Up")
                progressDialog.setMessage("Please wait, this may take a while...")
                progressDialog.setCanceledOnTouchOutside(false)
                progressDialog.show()
                val mAuth: FirebaseAuth=FirebaseAuth.getInstance()

                mAuth.createUserWithEmailAndPassword(email,password)
                    .addOnCompleteListener{task ->
                        if (task.isSuccessful)
                        {
                                saveUserInfo(fullName,username,email,password,progressDialog)
                        }
                        else
                        {
                            val message = task.exception!!.toString()
                            Toast.makeText(this,"Error $message",Toast.LENGTH_LONG).show()
                            mAuth.signOut()
                            progressDialog.dismiss()
                        }
                    }
            }
        }
    }

    private fun saveUserInfo(fullName: String, username: String, email: String, password: String,progressDialog:ProgressDialog) {

        val currentUserID=FirebaseAuth.getInstance().currentUser!!.uid
        val usersRef : DatabaseReference=FirebaseDatabase.getInstance().reference.child("Users")

        val userMap=HashMap<String,Any>()
        userMap["uid"]=currentUserID
        userMap["fullname"]=fullName.toLowerCase()
        userMap["username"]=username.toLowerCase()
        userMap["email"]=email
        userMap["bio"]="bio"
        userMap["image"]="https://firebasestorage.googleapis.com/v0/b/licenta-62ec2.appspot.com/o/Default%20Images%2Fprofile.png?alt=media&token=f6bf7335-3666-4536-bc5b-1c3c27ba6e4d"

        usersRef.child(currentUserID).setValue(userMap)
            .addOnCompleteListener { task ->
                if(task.isSuccessful)
                {
                        progressDialog.dismiss()
                        Toast.makeText(this,"Account has been created successfully",Toast.LENGTH_LONG).show()


                        FirebaseDatabase.getInstance().reference
                                .child("Follow").child(currentUserID)
                                .child("Following").child(currentUserID)
                                .setValue(true)

                        val intent =Intent(this@SignUpActivity,MainActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        finish()
                }
                else
                {
                    val message = task.exception!!.toString()
                    Toast.makeText(this,"Error $message",Toast.LENGTH_LONG).show()
                    FirebaseAuth.getInstance().signOut()
                    progressDialog.dismiss()
                }
            }

    }
}