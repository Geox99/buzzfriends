package com.example.licenta

import android.content.Intent
import android.graphics.Color
import android.graphics.LinearGradient
import android.graphics.Shader
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.android.synthetic.main.fragment_home.*

class SplashActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)


        val topAnimation=AnimationUtils.loadAnimation(this,R.anim.top_animation)
        val bottomAnimation=AnimationUtils.loadAnimation(this,R.anim.bottom_animation)

        app_icon.startAnimation(topAnimation)
        logo.startAnimation(bottomAnimation)

        val splashScreenTimeOut=5000
        val introIntent= Intent(this@SplashActivity,IntroActivity::class.java)

        val paint = logo.paint
        val width = paint.measureText(logo.text.toString())
        val textShader: Shader = LinearGradient(0f, 0f, width,logo.textSize, intArrayOf(
                Color.parseColor("#d9d0d0"),
                Color.parseColor("#e63737"),
                Color.parseColor("#cc0a0a")
        ), null, Shader.TileMode.REPEAT)

        logo.paint.shader = textShader

        Handler().postDelayed({
            startActivity(introIntent)
            finish()
        },splashScreenTimeOut.toLong())

    }
}