package com.example.licenta.fragments

import android.content.Context
import android.content.Intent
import android.media.Image
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.licenta.AccountSettingsActivity
import com.example.licenta.Adapter.MyImagesAdapter
import com.example.licenta.Model.Post
import com.example.licenta.Model.User
import com.example.licenta.R
import com.example.licenta.ShowUsersActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import java.util.*
import kotlin.collections.ArrayList


class ProfileFragment : Fragment() {

    private lateinit var profileId :String
    private lateinit var firebaseUser: FirebaseUser

    var postList: List<Post>?= null
    var myImagesAdapter: MyImagesAdapter?= null
    var myImagesAdapterSavedImg: MyImagesAdapter?= null
    var postListSaved: List<Post>?= null
    var mySavedImg: List<String>?= null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_profile, container, false)

        val pref=context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)
        firebaseUser=FirebaseAuth.getInstance().currentUser!!


        if(pref!= null)
        {
            this.profileId= pref.getString("profileId","none").toString()
        }

        if(profileId==firebaseUser.uid)
        {
            view.edit_account_settings_btn.text="Edit Profile"
        }
        else if (profileId!=firebaseUser.uid)
        {
            checkFollowAndFollowingButtonStatus()
        }
        //recycler View for Uploaded Images
        var recyclerViewUploadImages :RecyclerView
        recyclerViewUploadImages=view.findViewById(R.id.recycler_view_upload_pic)
        recyclerViewUploadImages.setHasFixedSize(true)
        val linearLayoutManager: LinearLayoutManager=GridLayoutManager(context,3)
        recyclerViewUploadImages.layoutManager=linearLayoutManager

        postList=ArrayList()
        myImagesAdapter=context?.let { MyImagesAdapter (it,postList as ArrayList<Post> ) }
        recyclerViewUploadImages.adapter=myImagesAdapter

        //recycler View for Saved Images
        var recyclerViewUSavedImages :RecyclerView
        recyclerViewUSavedImages=view.findViewById(R.id.recycler_view_saved_pic)
        recyclerViewUSavedImages.setHasFixedSize(true)
        val linearLayoutManager2: LinearLayoutManager=GridLayoutManager(context,3)
        recyclerViewUSavedImages.layoutManager=linearLayoutManager2

        postListSaved=ArrayList()
        myImagesAdapterSavedImg=context?.let { MyImagesAdapter (it,postListSaved as ArrayList<Post> ) }
        recyclerViewUSavedImages.adapter=myImagesAdapterSavedImg

        recyclerViewUSavedImages.visibility=View.GONE
        recyclerViewUploadImages.visibility=View.VISIBLE

        var uploadedImagesBtn: ImageButton
        uploadedImagesBtn=view.findViewById(R.id.images_grid_view_btn)
        uploadedImagesBtn.setOnClickListener{
            recyclerViewUSavedImages.visibility=View.GONE
            recyclerViewUploadImages.visibility=View.VISIBLE
        }

        var savedImagesBtn: ImageButton
        savedImagesBtn=view.findViewById(R.id.images_save_btn)
        savedImagesBtn.setOnClickListener{
            recyclerViewUSavedImages.visibility=View.VISIBLE
            recyclerViewUploadImages.visibility=View.GONE
        }

        view.total_followers.setOnClickListener {
            val intent =Intent(context,ShowUsersActivity::class.java)
            intent.putExtra("id",profileId)
            intent.putExtra("title","followers")
            startActivity(intent)
        }

        view.total_following.setOnClickListener {
            val intent =Intent(context,ShowUsersActivity::class.java)
            intent.putExtra("id",profileId)
            intent.putExtra("title","following")
            startActivity(intent)
        }



        view.edit_account_settings_btn.setOnClickListener{

            val getButtonText =view.edit_account_settings_btn.text.toString()

            when
            {
                getButtonText=="Edit Profile" ->startActivity(Intent(context,AccountSettingsActivity::class.java))

                getButtonText=="Follow" ->{
                    firebaseUser?.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                                .child("Follow").child(it1.toString())
                                .child("Following").child(profileId).setValue(true)
                    }

                    firebaseUser?.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                                .child("Follow").child(profileId)
                                .child("Followers").child(it1.toString()).setValue(true)
                    }
                    addNotification()
                }

                getButtonText=="Following" ->{
                    firebaseUser?.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                                .child("Follow").child(it1.toString())
                                .child("Following").child(profileId).removeValue()
                    }

                    firebaseUser?.uid.let { it1 ->
                        FirebaseDatabase.getInstance().reference
                                .child("Follow").child(profileId)
                                .child("Followers").child(it1.toString()).removeValue()
                    }
                }
            }

        }

        getFollowers()
        getFollowings()
        userInfo()
        myPhotos()
        getTotalNumberOfPosts()
        mySaves()

        return view
    }



    private fun checkFollowAndFollowingButtonStatus() {
        val followingRef= firebaseUser?.uid.let { it1 ->
            FirebaseDatabase.getInstance().reference
                    .child("Follow").child(it1.toString())
                    .child("Following")


        }
        if(followingRef!=null)
        {
            followingRef.addValueEventListener(object :ValueEventListener{
                override fun onDataChange(datasnapshot: DataSnapshot) {
                    if(datasnapshot.child(profileId).exists())
                    {
                        view?.edit_account_settings_btn?.text="Following"
                    }
                    else
                    {
                        view?.edit_account_settings_btn?.text="Follow"
                    }
                }

                override fun onCancelled(error: DatabaseError) {

                }
            })
        }
    }

    private fun getFollowers()
    {
        val followersRef= FirebaseDatabase.getInstance().reference
                    .child("Follow").child(profileId)
                    .child("Followers")

        followersRef.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                if(snapshot.exists())
                {
                    view?.total_followers?.text=snapshot.childrenCount.toString()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun getFollowings()
    {
        val followingsRef= FirebaseDatabase.getInstance().reference
                    .child("Follow").child(profileId)
                    .child("Following")

        followingsRef.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                if(snapshot.exists())
                {
                    view?.total_following?.text=snapshot.childrenCount.toString()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun myPhotos()
    {
        val postsRef=FirebaseDatabase.getInstance().reference.child("Posts")
        postsRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists())
                {
                    (postList as ArrayList<Post>).clear()
                    for(datasnapshot in snapshot.children)
                    {
                        val post=datasnapshot.getValue(Post::class.java)!!
                        if(post.getPublisher().equals(profileId))
                        {
                            (postList as ArrayList<Post>).add(post)
                        }
                        Collections.reverse(postList)
                        myImagesAdapter!!.notifyDataSetChanged()

                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }


    private fun userInfo()
    {
        val usersRef=FirebaseDatabase.getInstance().getReference().child("Users").child(profileId)

        usersRef.addValueEventListener(object :ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists())
                {
                    val user=snapshot.getValue<User>(User::class.java)

                    Picasso.get().load(user!!.getImage()).placeholder(R.drawable.profile).into(view?.pro_image_profile_frag)
                    view?.profile_fragment_username?.text=user!!.getUsername()
                    view?.full_name_profile_frag?.text=user!!.getFullname()
                    view?.bio_profile_frag?.text=user!!.getBio()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    override fun onStop() {
        super.onStop()

        val pref=context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId",firebaseUser.uid)
        pref?.apply()
    }

    override fun onPause() {
        super.onPause()

        val pref=context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId",firebaseUser.uid)
        pref?.apply()
    }

    override fun onDestroy() {
        super.onDestroy()

        val pref=context?.getSharedPreferences("PREFS",Context.MODE_PRIVATE)?.edit()
        pref?.putString("profileId",firebaseUser.uid)
        pref?.apply()
    }

    private fun getTotalNumberOfPosts()
    {
        val postsRef=FirebaseDatabase.getInstance().reference.child("Posts")
        postsRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(snapshot: DataSnapshot) {

                if(snapshot.exists())
                {
                    var postCounter=0
                    for(dataSnapshot in snapshot.children)
                    {
                        val post =dataSnapshot.getValue(Post::class.java)!!
                        if(post.getPublisher()==profileId)
                        {
                            postCounter++
                        }
                    }
                    total_post.text=" "+postCounter
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun mySaves() {
        mySavedImg=ArrayList()
        val savedRef=FirebaseDatabase.getInstance().reference.child("Saves").child(firebaseUser.uid)

        savedRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if(datasnapshot.exists())
                {
                    for(snapshot in datasnapshot.children)
                    {
                        (mySavedImg as ArrayList<String>).add(snapshot.key!!)
                    }
                    readSavedImagesData()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun readSavedImagesData() {

        val postsRef=FirebaseDatabase.getInstance().reference.child("Posts")

        postsRef.addValueEventListener(object : ValueEventListener{
            override fun onDataChange(datasnapshot: DataSnapshot) {
                if(datasnapshot.exists())
                {
                    (postListSaved as ArrayList<Post>).clear()

                    for(snapshot in datasnapshot.children)
                    {
                        val post =snapshot.getValue(Post::class.java)
                        for (key in mySavedImg!!)
                        {
                            if(post!!.getPostid()==key)
                            {
                                (postListSaved as ArrayList<Post>).add(post!!)
                            }
                        }
                    }
                    myImagesAdapterSavedImg!!.notifyDataSetChanged()
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun addNotification()
    {
        val notiRef=FirebaseDatabase.getInstance().reference.child("Notifications")
                .child(profileId)

        val notiMap =HashMap<String,Any>()
        notiMap["userid"]=firebaseUser!!.uid
        notiMap["text"]="started following you"
        notiMap["postid"]=""
        notiMap["ispost"]=false

        notiRef.push().setValue(notiMap)

    }

}