package com.example.licenta

import android.app.Activity
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.licenta.Model.User
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.StorageTask
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso
import com.theartofdev.edmodo.cropper.CropImage
import kotlinx.android.synthetic.main.activity_account_settings.*

class AccountSettingsActivity : AppCompatActivity() {

    private lateinit var firebaseUser: FirebaseUser
    private var checker =""
    private var myUrl= ""
    private var imageUri: Uri?= null
    private var storageProfilePictureRef :StorageReference ? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_settings)

        firebaseUser=FirebaseAuth.getInstance().currentUser!!
        storageProfilePictureRef=FirebaseStorage.getInstance().reference.child("Profile Pictures")

        logout_btn.setOnClickListener {
            FirebaseAuth.getInstance().signOut()

            val intent = Intent(this@AccountSettingsActivity,SignInActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }

        change_image_text_btn.setOnClickListener {

            checker="clicked"

            CropImage.activity()
                    .setAspectRatio(1,1)
                    .start(this@AccountSettingsActivity)
        }

        save_info_profile_btn.setOnClickListener {
            if(checker== "clicked")
            {
                uploadImageAndUpdateInfo()
            }
            else
            {
                updateUserInfoOnly()
            }

        }

        close_profile_btn.setOnClickListener {
           val intent=Intent(this@AccountSettingsActivity,MainActivity::class.java)
            startActivity(intent)
            finish()
        }
        delete_account_btn.setOnClickListener {
            val alertDialog= AlertDialog.Builder(this@AccountSettingsActivity).create()
            alertDialog.setTitle("Are you sure? ")
            alertDialog.setMessage("Deleting this account will result in completely removing your account from the system and you won't be able to access the app")
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,"Delete")
            {
                dialogInterface, which ->
                firebaseUser.delete().addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this@AccountSettingsActivity, "Account Deleted", Toast.LENGTH_LONG).show()
                        val intent = Intent(this@AccountSettingsActivity, SignUpActivity::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast.makeText(this@AccountSettingsActivity, task.exception?.message, Toast.LENGTH_LONG).show()
                    }
                    dialogInterface.dismiss()
                }
            }
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Dismiss")
            {
                dialogInterface, which ->
                dialogInterface.dismiss()
            }


                alertDialog.show()
            val positiveButton: Button = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
            val negativeButton: Button= alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE)

            positiveButton.setTextColor(Color.parseColor("#DA031C"))
            negativeButton.setTextColor(Color.parseColor("#DA031C"))


            }



        userInfo()
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode==CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode==Activity.RESULT_OK && data !=null)
        {
            val result =CropImage.getActivityResult(data)
            imageUri=result.uri
            profile_image_view_frag.setImageURI(imageUri)


        }
    }

    private fun updateUserInfoOnly() {

        when {
            full_name_profile_frag.editText?.text.toString()=="" -> {
                Toast.makeText(this,"Please write full name first", Toast.LENGTH_LONG).show()
            }
            username_profile_frag.editText?.text.toString()=="" -> {
                Toast.makeText(this,"Please write username first", Toast.LENGTH_LONG).show()
            }
            bio_profile_frag.editText?.text.toString()=="" -> {
                Toast.makeText(this,"Please write your  bio first", Toast.LENGTH_LONG).show()
            }
            else -> {
                val usersRef= FirebaseDatabase.getInstance().reference.child("Users")
                val userMap=HashMap<String,Any>()
                userMap["fullname"]=full_name_profile_frag.editText?.text.toString().toLowerCase()
                userMap["username"]=username_profile_frag.editText?.text.toString().toLowerCase()
                userMap["bio"]=bio_profile_frag.editText?.text.toString().toLowerCase()

                usersRef.child(firebaseUser.uid).updateChildren(userMap)

                Toast.makeText(this,"Account Information has been updated successfully",Toast.LENGTH_LONG).show()

                val intent =Intent(this@AccountSettingsActivity,MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }


    private fun userInfo()
    {
        val usersRef= FirebaseDatabase.getInstance().getReference().child("Users").child(firebaseUser.uid)

        usersRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if(snapshot.exists())
                {
                    val user=snapshot.getValue<User>(User::class.java)

                    Picasso.get().load(user!!.getImage()).placeholder(R.drawable.profile).into(profile_image_view_frag)
                    username_profile_frag.editText?.setText(user!!.getUsername())
                    full_name_profile_frag.editText?.setText(user!!.getFullname())
                    bio_profile_frag.editText?.setText(user!!.getBio())
                }
            }

            override fun onCancelled(error: DatabaseError) {

            }
        })
    }

    private fun uploadImageAndUpdateInfo() {




        when {
            imageUri==null ->{
                Toast.makeText(this, "Please select your image first", Toast.LENGTH_LONG).show()
            }
            full_name_profile_frag.editText?.text.toString() == "" -> {
                Toast.makeText(this, "Please write full name first", Toast.LENGTH_LONG).show()
            }
            username_profile_frag.editText?.text.toString() == "" -> {
                Toast.makeText(this, "Please write username first", Toast.LENGTH_LONG).show()
            }
            bio_profile_frag.editText?.text.toString() == "" -> {
                Toast.makeText(this, "Please write your  bio first", Toast.LENGTH_LONG).show()
            }
            else ->
            {
                val progressDialog=ProgressDialog(this)
                progressDialog.setTitle("Account Settings")
                progressDialog.setMessage("Please wait, we are updating your profile...")
                progressDialog.show()

                val fileRef =storageProfilePictureRef!!.child(firebaseUser!!.uid+"jpg")

                val uploadTask: StorageTask<*>
                uploadTask=fileRef.putFile(imageUri!!)
                uploadTask.continueWithTask(Continuation <UploadTask.TaskSnapshot, Task<Uri>>{ task->
                    if (!task.isSuccessful)
                    {
                        task.exception?.let{
                            throw it
                            progressDialog.dismiss()
                        }
                    }
                    return@Continuation fileRef.downloadUrl
                }).addOnCompleteListener ( OnCompleteListener<Uri>{task ->
                    if(task.isSuccessful)
                    {
                        val downloadUri =task.result
                        myUrl=downloadUri.toString()

                        val ref=FirebaseDatabase.getInstance().reference.child("Users")
                        val userMap=HashMap<String,Any>()
                        userMap["fullname"]=full_name_profile_frag.editText?.text.toString().toLowerCase()
                        userMap["username"]=username_profile_frag.editText?.text.toString().toLowerCase()
                        userMap["bio"]=bio_profile_frag.editText?.text.toString().toLowerCase()
                        userMap["image"]=myUrl
                        ref.child(firebaseUser.uid).updateChildren(userMap)

                        Toast.makeText(this,"Account Information has been updated successfully",Toast.LENGTH_LONG).show()

                        val intent =Intent(this@AccountSettingsActivity,MainActivity::class.java)
                        startActivity(intent)
                        finish()
                        progressDialog.dismiss()

                    }
                    else{
                        progressDialog.dismiss()
                    }
                } )
            }
        }

    }

}